---
layout: markdown_page
title: "What are the benefits of continuous integration?"
description: "Learn the benefits of continuous integration and see why organizations that use continuous integration have a competitive advantage over those that don’t."
---
## What are the benefits of continuous integration?

[Continuous integration (CI)](/topics/ci-cd/) makes software development easier, faster, and less risky for developers. By automating builds and tests, developers can make smaller changes and commit them with confidence. Developers get feedback on their code sooner, increasing the overall pace of innovation. 

Organizations that adopt continuous integration have [a competitive advantage](https://about.gitlab.com/blog/2019/06/27/positive-outcomes-ci-cd/) because of the ability to deploy faster. Organizations that have implemented CI are making revenue on the features they deploy, not waiting for manual code checks. 

Studies done by DevOps Research and Assessment (DORA) have shown that robust DevOps practices lead to improved <a href="https://cloud.google.com/devops/state-of-devops/" target="_blank">business outcomes</a>. All of these DORA 4 metrics can be improved by using CI:

*   **Lead time**: Early feedback and build/test automation help decrease the time it takes to go from code committed to code successfully running in production.
*   **Deployment frequency**: Automated tests and builds are a pre-requisite to automated deploy.
*   **Time to restore service**: Automated pipelines enable fixes to be deployed to production faster reducing Mean Time to Resolution (MTTR).
*   **Change failure rate**: Early automated testing greatly reduced the number of defects that make their way out to production.


## The business benefits of continuous integration

With less manual work, DevOps teams work more efficiently and with greater speed. An automated workflow also improves handoffs, which enhances overall operational efficiency. The business benefits of continuous integration allow organizations to:

*   **Iterate faster**: Smaller code changes allow teams to iterate faster and are easier to manage.
*   **Find problems easily**: Teams can find problems in code because all code is managed and tested in smaller batches.
*   **Have better transparency**: Continuous feedback through frequent testing helps developers see where bugs are.
*   **Reduce costs**: Automated testing frees up time for developers by reducing manual tasks, and better code quality means fewer errors and less downtime.
*   **Make users happy**: Fewer bugs/errors make it into production, so users have a better experience.

Automated testing reduces the chances of human error and ensures that only code that meets certain standards makes it into production. Because code is tested in smaller batches, there’s less context-switching for developers when a bug/error occurs. Pipelines can also identify _where_ the error occurs, making it easier to not only identify problems, but fix them.

A dev environment with less manual tasks means that engineers can spend more time on revenue-generating projects. With fewer errors, teams are more efficient and spend less time putting out fires. When processes, such as unit testing, are automated, engineers are happier and can focus on where they add the most value.

Continuous integration and continuous delivery bring [automation into the DevOps lifecycle](/blog/2019/04/25/5-teams-that-made-the-switch-to-gitlab-ci-cd/). DevOps teams work more efficiently and with greater speed with minimal manual work. An automated workflow also reduces the chances of human error and improves handoffs, which increases overall operational efficiency. Organizations that implement CI/CD make better use of their resources, are more cost efficient, and allow developers to focus on innovation.


## More on continuous integration

[Continuous integration with GitLab](/stages-devops-lifecycle/continuous-integration/)

[The benefits of a single application CI/CD](/resources/ebook-single-app-cicd/)

[How Hotjar deploys 50% faster with GitLab](/customers/hotjar/)


