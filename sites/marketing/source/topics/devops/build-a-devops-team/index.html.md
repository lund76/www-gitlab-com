---
layout: markdown_page
title: "Create the ideal DevOps team structure"
description: The DevOps platform is in place but is the team structure ready for prime time? Here’s what you need to consider when building a DevOps team.
---

A solid DevOps platform needs a solid DevOps team structure to achieve maximum efficiency. But finding the right balance in your DevOps team is not a one-size-fits-all proposition.

Several factors come into play when it comes to team structure:

* Existing silos: Are there product sets/teams that work independently?

* Technical leadership: Are group managers set up to achieve DevOps goals?

* Changing roles: Ops tasks have bled into dev roles, security teams are working with everyone, and technology is changing. Expect to regularly re-evaluate everything.

* Continuous improvement: A DevOps team will never be a “one and done.” Iteration will be required.

## Types of silos

Management consultant Matthew Skelton writes about a <a href="https://blog.matthewskelton.net/2013/10/22/what-team-structure-is-right-for-devops-to-flourish/" target="_blank">number of different DevOps scenarios</a> in great detail, but we’ll discuss just a few of the silos he mentions specifically and how they impact an organization.

## Dev and ops are completely separate

Skelton refers to this as a classic “throw it over the wall” team structure and, as implied, it’s not the most effective DevOps strategy. Both teams work in their bubbles and lack visibility into the workflow of the other team. This complete separation lacks collaboration, visibility, and understanding – vital components of what effective DevOps should be. What happens is essentially blame-shifting: "We don’t know what they are doing over there, we did our part and now it's up to them to complete it," and so on.

## DevOps middleman

In this team structure, there are still separate dev and ops teams, but there is now a “DevOps” team that sits between, as a facilitator of sorts. This is not necessarily a bad thing and Skelton stresses that this arrangement has some use cases. For example, if this is a temporary solution with the goal being to make dev and ops more cohesive in the future, it could be a good interim strategy.

## Ops stands alone

In this scenario, dev and DevOps are melded together while ops remains siloed. Organizations like this still see ops as something that supports the initiatives for software development, not something with value in itself. Organizations like this suffer from basic operational mistakes and could be much more successful if they understand the value ops brings to the table.

## What can DevOps team leadership do?

To break down DevOps team silos requires leadership at all levels. Start by asking each group to surface the major areas of friction and then identify leaders in each group – dev, ops, security, test. Each leader should work individually and together on all of the friction points. 

The importance of communication can't be overstated: Teams need to hear regular feedback about all aspects of their roles.

It might also be helpful to insert "champions" into struggling groups; they can model behaviors and language that facilitate communication and collaboration. 

## DevOps roles are blurring

Technology advances from multicloud to microservices and containers also play a role when it comes to defining the right DevOps team structure. In our [2020 Global DevSecOps Survey](/developer-survey/), 83% of respondents said their teams are releasing code more quickly but they also told us their roles were changing, dramatically in some cases.

Devs today are creating, monitoring, and maintaining infrastructures, roles that were traditionally the province of ops pros. Ops are spending more time managing cloud services, while security team members are working on cross-functional teams with dev and ops more than ever before. 

Obviously the software development lifecycle today is full of moving parts, meaning that defining the right structure for a DevOps team will remain fluid and in need of regular re-evaluation. 

## Remember to iterate

At GitLab [iteration]/handbook/values/#iteration) is one of our core values. And it’s something we practice a lot when it comes to our own DevOps team structure. Since GitLab is a complete DevOps platform delivered as a single application, our dev teams are organized into stages (e.g. [Verify](/handbook/engineering/development/ops/verify/), etc.) because these would be separate products at any other company and require their own autonomy. We also have other functional DevOps groups besides "Dev" that manage other aspects of our product. We have a [reliability group](/handbook/engineering/infrastructure/team/reliability/) that manages uptime and reliability for GitLab.com, a [quality department](/handbook/engineering/quality/), and a [distribution team](/handbook/engineering/development/enablement/distribution/), just to name a few. The way that we make all these pieces fit together is through our commitment to transparency and our visibility through the entire SDLC. But we also tweak (i.e. iterate on) this structure regularly to make everything work.

The bottom line: Plan to build your DevOps team, and then re-think it, and re-think it some more. The benefits in faster code releases and happier team members will make it worthwhile.

**Read more about the DevOps platform:**

What our [2020 Global DevSecOps Survey found](/developer-survey/)

DevOps is causing [role changes](/blog/2020/10/20/software-developer-changing-role/). What will this mean for you?

Here's why you [need a security champions program](/blog/2020/10/14/why-security-champions/)
