---
layout: handbook-page-toc
title: "Kano Model and Feature Prioritization Survey"
description: "Kano model provides a simple and powerful way how to think about the features that we plan to build."
---


## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Resources
- Article [The Complete Guide to Kano Model](https://foldingburritos.com/kano-model/) by Daniel Zacarias
- Article [Kano Model — Ways to use it and NOT use it](https://medium.com/design-ibm/kano-model-ways-to-use-it-and-not-use-it-1d205a9cf808) by Cary-Anne Olsen-Landis
- Video [Building a Winning UX Strategy Using the Kano Model](https://www.youtube.com/watch?v=Hr1rN3jibIk&feature=youtu.be) by Jared Spool

## Example research in GitLab
- [Survey for monitoring feature prioritization](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/28)
- [Survey for CI feature prioritization](https://gitlab.com/gitlab-org/ux-research/-/issues/1027)
